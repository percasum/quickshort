/*
	Name:
	Copyright: Omae wa mou shindiru NANI?
	Author: Cesar Marin, Pere Barbosa
	Date: 18/5/2018
	Description:Implementeu un programa (dins del fitxer randomFile.cpp) tal que crei N valors aleatoris
    i els vagi emmagatzemant fins del fitxer initialValues.txt. El valor N també serà un valor aleatori de
    tipus enter dins del rang [10, 10000]. La resta de valors aleatoris seran valors float dins del rang
    [-500.0, 30000.0]. El fitxer tindrà l'aspecte següent:
*/

#include <fstream>
#include <cstdlib>
#include <ctime>

void init(){
    int VSIZE;
    srand(time(NULL));
    VSIZE=(rand()%6-10)+10;
    //VSIZE=(rand()%10001-10)+10;
    std::fstream initialValues("initialValues.txt", std::fstream::out);//obertura del fitxer
    initialValues<<VSIZE<< std::endl;//escritura al fitxer
    for(int i=0;i<VSIZE;i++)
    {
        initialValues<<(rand()*30501.0)/RAND_MAX-500.0<<std::endl;
    }
    initialValues.close();
}

int main(){
    init();
}
