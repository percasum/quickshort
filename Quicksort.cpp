/*
	Name:
	Copyright: Omae wa mou shindiru NANI?
	Author: Cesar Marin, Pere Barbosa
	Date: 18/5/2018
	Description: Implementeu el programa d'ordenació (dins del fitxer quicksort.cpp).
    Aquest programa ha de fer el següent: Crear un vector dinàmic tal que, s'inicialitzarà
    amb els valors indicats dins del fitxer initialValues.txt creat en el punt anterior.
    És a dir, tindrà una mida dinàmica de N i els elements que contindrà seran els valors
    float aleatoris n1, n2, ..., nN.
*/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <string>

void read(float v[], int &VSIZE){
    std::fstream initialValues("initialValues.txt", std::fstream::in);
    int i=0;
    std::string line;
    getline(initialValues, line);
    VSIZE=atoi(line.c_str());
    v=new float[VSIZE];
    while(initialValues.eof()==false){
        getline(initialValues,line);
        v[i]=atoi(line.c_str());
        i++;
    }
    initialValues.close();
}

void orden(float v[],int min, int max){
    float aux;
    int pivot;
    if(min==max || min>max)
        return;
    else{
        pivot=min;
        int imax=max+1;
        for(int i=pivot+1;i<=max;i++)
        {
            if(v[pivot] < v[i] && imax == max+1){
                imax=i;
            }else if (v[pivot] >= v[i] && imax != max+1){
            aux=v[i];
            v[i]=v[imax];
            v[imax]=aux;
            imax++;
            }

        }
        aux=v[pivot];
        v[pivot]=v[imax-1];
        v[imax-1]=aux;
        orden(v,min,imax-2);
        orden(v,imax,max);
    }
}
void write(float v[], int VSIZE){
    std::fstream Resultats("quicksortResult.txt", std::fstream::out);
    for(int i=0; i<VSIZE;i++){
      std::cout<<v[i]<<std::endl;
        Resultats<<v[i] << std::endl;
    }
    Resultats.close();
}

int main(){
    int VSIZE;
    float *v;
    read(v,VSIZE);
    orden(v,0,VSIZE-1);
    //delete
    delete []v;
}
