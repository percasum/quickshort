Pere Barbosa, César Marín


Precondició: Creem dos fitxers cpp, randomFile.cpp i Quicksort.cpp, 
al primer crearem un  programa per a que doni valors aleatoris a una variable
que serà el tamany del vector amb nombres dintre del rang [10, 10000],
i els valors de cada posició sran entre [-500.0, 30000.0], aquests valors es 
guardaran al fitxer initialValues.txt.

Postcondició: Al fitxer del Quicksort crearem un vector dinamic amb el tamany
que ens habia sortit al random i amb els valors del initialValues.txt, desprès
mostrem els valors del vector, i finalment els ordenem amb la funció del Quicksort,
i guardarem el resultat al fitxer quicksortResult.txt i també ho mostrarem per
pantalla.